﻿using System;
using JWT;
using JWT.Algorithms;
using JWT.Builder;

namespace jwtgen
{
	class Program
	{
		static void Main(string[] args)
		{
			string x;
			
			var token = new JwtBuilder()
				.WithAlgorithm(new HMACSHA256Algorithm())
				.WithUrlEncoder(new JwtBase64UrlEncoder())
				.WithSecret("hello")
				.Issuer("example.com")
				.AddClaim("exp", DateTimeOffset.UtcNow.AddMinutes(1).ToUnixTimeSeconds())
				//.ExpirationTime(DateTime.UtcNow.AddMinutes(50))
				
				.AddClaim("user", "toto")
				.AddClaim("file", "air_hover1.png")
				.Build();

			Console.WriteLine(token);
			Console.ReadKey();
		}
	}
}
